var objectBucketName = "wwwprasunS3";
var bucketRegion = "ap-south-1";
var IdentityPoolId = "myId";

AWS.config.update({
    region: bucketRegion,
    credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: IdentityPoolId
    })
});

var s3 = new AWS.S3({
    params: { Bucket: objectBucketName }
});

function listObjects() {
    return s3.listObjects({ Delimiter: "/" }, function(err, data) {
        if (err) {
            return alert("There was an error listing your objects: " + err.message);
        } else {
            return data.CommonPrefixes.map(function(commonPrefix) {
                var prefix = commonPrefix.Prefix;
                var object = decodeURIComponent(prefix.replace("/", ""));
                return object;
            });

        }
    });
}

function createObjects(objectName) {
    objectName = objectName.trim();
    if (!objectName) {
        return alert("Object names must contain at least one non-space character.");
    }
    if (objectName.indexOf("/") !== -1) {
        return alert("Object names cannot contain slashes.");
    }
    var objectKey = encodeURIComponent(objectName);
    return s3.headObject({ Key: objectKey }, function(err, data) {
        if (!err) {
            return alert("Object already exists.");
        }
        if (err.code !== "NotFound") {
            return alert("There was an error creating your object: " + err.message);
        }
        s3.putObject({ Key: objectKey }, function(err, data) {
            if (err) {
                return alert("There was an error creating your object: " + err.message);
            }
            alert("Successfully created object.");
            viewObject(objectName);
        });
    });
}

function viewObject(objectName) {
    var objectPhotosKey = encodeURIComponent(objectName) + "/";
    return s3.listObjects({ Prefix: objectPhotosKey }, function(err, data) {
        if (err) {
            return alert("There was an error viewing your object: " + err.message);
        }
        // 'this' references the AWS.Response instance that represents the response
        var href = this.request.httpRequest.endpoint.href;
        var bucketUrl = href + objectBucketName + "/";

        return data.Contents.map(function(photo) {
            var photoKey = photo.Key;
            var photoUrl = bucketUrl + encodeURIComponent(photoKey);
            return photoUrl
        });
    });
}

function addPhoto(objectName) {
    var files = document.getElementById("photoupload").files;
    if (!files.length) {
        return alert("Please choose a file to upload first.");
    }
    var file = files[0];
    var fileName = file.name;
    var objectPhotosKey = encodeURIComponent(objectName) + "/";

    var key = objectPhotosKey + fileName;

    // Use S3 ManagedUpload class as it supports multipart uploads
    var upload = new AWS.S3.ManagedUpload({
        params: {
            Bucket: objectBucketName,
            Key: key,
            Body: file
        }
    });

    var promise = upload.promise();

    promise.then(
        function(data) {
            alert("Successfully uploaded photo.");
            viewObject(objectName);
        },
        function(err) {
            return alert("There was an error uploading your photo: ", err.message);
        }
    );
}

function deletePhoto(objectName, photoKey) {
    return s3.deleteObject({ Key: photoKey }, function(err, data) {
        if (err) {
            return alert("There was an error deleting your photo: ", err.message);
        }
        alert("Successfully deleted photo.");
        viewObject(objectName);
    });
}

function deleteObject(objectName) {
    var objectKey = encodeURIComponent(objectName) + "/";
    return s3.listObjects({ Prefix: objectKey }, function(err, data) {
        if (err) {
            return alert("There was an error deleting your object: ", err.message);
        }
        var objects = data.Contents.map(function(object) {
            return { Key: object.Key };
        });
        s3.deleteObjects({
                Delete: { Objects: objects, Quiet: true }
            },
            function(err, data) {
                if (err) {
                    return alert("There was an error deleting your object: ", err.message);
                }
                alert("Successfully deleted object.");
                listObjects();
            }
        );
    });
}