var AWS = require('aws-sdk');
AWS.config.update({ region: "ap-south-1" });


export function listTemplates() {
    var templatePromise = new AWS.SES({ apiVersion: '2010-12-01' }).listTemplates({ MaxItems: 3 }).promise();

    templatePromise.then(
        function(data) {
            console.log(data);
        }).catch(
        function(err) {
            console.error(err, err.stack);
        });
}
export function getTemplate() {
    var templatePromise = new AWS.SES({ apiVersion: '2010-12-01' }).getTemplate({ TemplateName: 'FirstEmailTemplate' }).promise();

    templatePromise.then(
        function(data) {
            console.log(data.Template.SubjectPart);
        }).catch(
        function(err) {
            console.error(err, err.stack);
        });

}

export function createTemplate() {

    var params = {
        Template: {
            TemplateName: 'ThirdEmailTemplate',
            HtmlPart: '<h1>Hello SES</h1?',
            SubjectPart: 'This is a subject',
            TextPart: 'This is a text'
        }
    };

    var templatePromise = new AWS.SES({ apiVersion: '2010-12-01' }).createTemplate(params).promise();

    templatePromise.then(
        function(data) {
            console.log(data);
        }).catch(
        function(err) {
            console.error(err, err.stack);
        });

}
export function sendmailTemplate() {
    var params = {
        Destination: {
            CcAddresses: [
                'prasuntest@yopmail.com',
                'prasuntest2@yopmail.com',
            ],
            ToAddresses: [
                'toprasun@yopmail.com'
            ]
        },
        Source: 'prasunatwork@gmail.com',
        Template: 'ThirdEmailTemplate',
        TemplateData: '{}',
        ReplyToAddresses: [
            'prasunatwork@gmail.com'
        ],
    };

    var sendPromise = new AWS.SES({ apiVersion: '2010-12-01' }).sendTemplatedEmail(params).promise();

    sendPromise.then(
        function(data) {
            console.log(data);
        }).catch(
        function(err) {
            console.error(err, err.stack);
        });

}